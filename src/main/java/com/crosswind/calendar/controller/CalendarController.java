/*************************************************************************
 *
 * ROUTINIFY CONFIDENTIAL
 * __________________
 *
 * [2018] - [2019] Routinify, Inc.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Routinify, Inc and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Routinify, Inc and its suppliers
 * and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Routinify, Inc.
 */

package com.crosswind.calendar.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.crosswind.calendar.pojo.EventRequest;
import com.crosswind.calendar.service.CalendarService;

@RestController
public class CalendarController 
{
	@Autowired
	private CalendarService calendarService;
	
	@RequestMapping(value = "v1/addCalendar", produces = "text/plain")
	public String addCalendarToSenior(HttpServletRequest request) throws Exception 
	{
		String email = request.getParameter("email");
		return calendarService.authorizeSeniorAndAssignCalendarId(email);
    }
	
	@RequestMapping(value = "v1/addEvent", produces = "text/plain", method = RequestMethod.POST)
	public String addEventToCalendar(@RequestBody EventRequest eventRequest) throws Exception 
	{
		calendarService.createEvent(eventRequest);
		return "Event Added";
    }

	//////////////////////////////////////////////////////////////////////////////
	// The following are methods that are basically proxid calls to 
	// the Google Calendar API
	//
	
    @RequestMapping(value = "v1/calendars", produces = "application/json",
            method = RequestMethod.POST)
    public ResponseEntity<?> createCalendar(@RequestBody Map<String, Object> calendarData) 
            throws Exception 
    {
        return calendarService.createCalendar(calendarData);
    }        	

    @RequestMapping(value = "v1/calendars/{calendarId}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteCalendar(@PathVariable ("calendarId") String calendarId) 
            throws Exception
    {
        return calendarService.deleteCalendar(calendarId);
    }
    
    @RequestMapping(value = "v1/calendars/{calendarId}/events", produces = "application/json",
            method = RequestMethod.POST)
    public ResponseEntity<?> addEvent(@PathVariable ("calendarId") String calendarId,
            @RequestBody Map<String, Object> eventData) 
            throws Exception 
    {
        return calendarService.createEvent(calendarId, eventData);
    }
		
	@RequestMapping(value = "v1/calendars/{calendarId}/events", produces = "application/json", method = RequestMethod.GET)
	public ResponseEntity<?> getEvents(@PathVariable ("calendarId") String calendarId,
			HttpServletRequest request) 
			        throws Exception
	{
		Map<String, String[]> params = request.getParameterMap();
		return calendarService.getEvents(calendarId, params);
	}
	
	@RequestMapping(value = "v1/calendars/{calendarId}/events/{eventId}", produces = "application/json", method = RequestMethod.GET)
	public ResponseEntity<?> getEvent(@PathVariable ("calendarId") String calendarId,
	        @PathVariable ("eventId") String eventId) 
	                throws Exception
	{
	    return calendarService.getEvent(calendarId, eventId);
	}

    @RequestMapping(value = "v1/calendars/{calendarId}/events/{eventId}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteEvent(@PathVariable ("calendarId") String calendarId,
            @PathVariable ("eventId") String eventId) 
                    throws Exception
    {
        return calendarService.deleteEvent(calendarId, eventId);
    }
}
