/*************************************************************************
 *
 * ROUTINIFY CONFIDENTIAL
 * __________________
 *
 * [2018] - [2019] Routinify, Inc.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Routinify, Inc and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Routinify, Inc and its suppliers
 * and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Routinify, Inc.
 */

package com.crosswind.calendar.serviceImpl;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;

import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.crosswind.calendar.pojo.EventRequest;
import com.crosswind.calendar.service.CalendarService;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpContent;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.json.JsonHttpContent;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.model.AclRule;
import com.google.api.services.calendar.model.AclRule.Scope;
import com.google.api.services.calendar.model.Calendar;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Event.Reminders;
import com.google.api.services.calendar.model.EventDateTime;
import com.google.api.services.calendar.model.EventReminder;

@Service
public class CalendarServiceImpl implements CalendarService 
{
	private Logger log_ = LoggerFactory.getLogger(CalendarServiceImpl.class);

	@Autowired
	private GoogleConfig googleConfig_;

    protected ObjectMapper mapper_;

    @PostConstruct
    void postConstruct() 
    {
        mapper_ = new ObjectMapper();
        mapper_.setSerializationInclusion(Include.NON_NULL);
        mapper_.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);
        mapper_.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper_.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    }   
	
	@Override
	public String authorizeSeniorAndAssignCalendarId(String email) throws Exception 
	{
		log_.info("Create new calendar for senior");
	      Calendar calendar = null;
	     try {
	    	// Create access rule with associated scope
	    	  AclRule rule = new AclRule();
	    	  Scope scope = new Scope();
	    	  scope.setType("user").setValue(email);
	    	  rule.setScope(scope).setRole("reader");
	    	  
	    	  calendar = addCalendar(googleConfig_.getGCalClient(), email);
	    	  // Insert new access rule
	    	  AclRule createdRule = googleConfig_.getGCalClient().acl().insert(calendar.getId(), rule).execute();
	    	  log_.info(createdRule.getId());
	    	  return calendar.getId();
		} catch (Exception e) {
			log_.error(e.getMessage());
			e.printStackTrace();
			throw e;
		}
	}
	
	public Calendar addCalendar(com.google.api.services.calendar.Calendar client, String email) throws IOException {
	    Calendar entry = new Calendar();
	    entry.setSummary(email);
	    entry.setTimeZone("UTC");
	    Calendar result = googleConfig_.getGCalClient().calendars().insert(entry).execute();
	    log_.info(result.toString());
	    return result;
	  }

	@Override
	public void createEvent(EventRequest eventRequest) throws Exception 
	{
		log_.info("Create new Event");
		Calendar calendar = getCalendarById(eventRequest.getCalendarId());
		calendar.setTimeZone("UTC");
		DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		format.setTimeZone(TimeZone.getTimeZone(eventRequest.getTimezone()));
		addEvent(calendar, eventRequest.getSummary(), eventRequest.getDescription(), format.parse(eventRequest.getStartdate()), format.parse(eventRequest.getEnddate()), eventRequest.getRrule());
	}
	
	private void addEvent(Calendar calendar, String summary, String description, Date startDate, Date endDate, String rrule) throws IOException {
		  log_.info("Add Event");
	    Event event = newEvent(summary, description, startDate, endDate, rrule);
	    Event result = googleConfig_.getGCalClient().events().insert(calendar.getId(), event).execute();
	    log_.info(result.toString());
	  }
	
	private Event newEvent(String summary, String description, Date startDate, Date endDate, String rrule) {
	    Event event = new Event();	    
	    event.setSummary(summary.trim());
	    event.setDescription(description.trim());
	    DateTime start = new DateTime(startDate, TimeZone.getTimeZone("UTC"));
	    event.setStart(new EventDateTime().setDateTime(start).setTimeZone("UTC"));
	    DateTime end = new DateTime(endDate, TimeZone.getTimeZone("UTC"));
	    event.setEnd(new EventDateTime().setDateTime(end).setTimeZone("UTC"));
	    event.setReminders(new Reminders()
	    		.setUseDefault(false)
	    		.setOverrides(Arrays.asList(new EventReminder()
	    						.setMethod("popup").setMinutes(5))));
	    String[] recurrence = new String[] {rrule};
	    event.setRecurrence(Arrays.asList(recurrence));
	    event.setConferenceData(null);
	    event.setGuestsCanModify(false);
	    event.setGuestsCanSeeOtherGuests(false);
	    event.setGuestsCanInviteOthers(false);
	    event.setLocked(true);
	    return event;
	  }
		
	private Calendar getCalendarById(String id) throws IOException {
	    Calendar calendar = new Calendar();
	    calendar = googleConfig_.getGCalClient().calendars().get(id).execute();
	    
	    return calendar;
	}

    /*
     * (non-Javadoc)
     * @see com.crosswind.calendar.service.CalendarService#createCalendar(java.util.Map)
     */
    @Override
    public ResponseEntity<?> createCalendar(Map<String, Object> calendarData) 
            throws IOException
    {
        GenericUrl url = new GenericUrl("https://www.googleapis.com/calendar/v3/calendars");

        HttpContent content = new JsonHttpContent(googleConfig_.getJsonFactory(), calendarData);
                
        if(log_.isDebugEnabled()) 
            log_.debug("Invoking Google Calendar POST API with url [{}], content [{}]",                    
                url.toString(), mapper_.writeValueAsString(calendarData));        
        
        HttpRequest request = googleConfig_.getRequestFactory().buildPostRequest(url, content);
        request.setReadTimeout(60000);
        
        HttpResponse response = request.execute();          
            
        Map<String, Object> jsonMap = processGCalResponse(response);
        if(log_.isDebugEnabled()) 
            log_.debug("Google Calendar response body [{}]", mapper_.writeValueAsString(jsonMap));
        
        return new ResponseEntity<>(jsonMap, HttpStatus.CREATED);        
    }
	
    /*
     * (non-Javadoc)
     * @see com.crosswind.calendar.service.CalendarService#deleteCalendar(java.lang.String)
     */
    @Override
    public ResponseEntity<?> deleteCalendar(String calendarId) 
            throws IOException
    {
        GenericUrl url = new GenericUrl("https://www.googleapis.com/calendar/v3/calendars/" + calendarId);

        if(log_.isDebugEnabled()) 
            log_.debug("Invoking Google Calendar DELETE API with url [{}]", url.toString());
        
        HttpRequest request = googleConfig_.getRequestFactory().buildDeleteRequest(url);
        request.execute();          
            
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /*
     * (non-Javadoc)
     * @see com.crosswind.calendar.service.CalendarService#getEvents(java.lang.String, java.util.Map)
     */
    public ResponseEntity<?> getEvents(String calendarId, Map<String, String[]> params) 
            throws IOException
    {
        GenericUrl url = new GenericUrl("https://www.googleapis.com/calendar/v3/calendars/" + 
                calendarId + "/events");
        
        for(Entry<String, String[]> entry : params.entrySet())
        {
            String[] values = entry.getValue();
            if(values == null)
                continue;
            
            for(String value : values)
                url.put(entry.getKey(), value);
        }

        if(log_.isDebugEnabled()) 
            log_.debug("Invoking Google Calendar GET API with url [{}]", url.toString());
        
        HttpRequest request = googleConfig_.getRequestFactory().buildGetRequest(url);
        HttpResponse response = request.execute();  	    
		    
        Map<String, Object> jsonMap = processGCalResponse(response);
        if(log_.isTraceEnabled()) 
            log_.trace("Google Calendar response body [{}]", jsonMap.toString());
        
        return new ResponseEntity<>(jsonMap, HttpStatus.OK);
    }

    /*
     * (non-Javadoc)
     * @see com.crosswind.calendar.service.CalendarService#getEvent(java.lang.String, java.lang.String)
     */
    @Override
    public ResponseEntity<?> getEvent(String calendarId, String eventId) 
            throws IOException
    {
        GenericUrl url = new GenericUrl("https://www.googleapis.com/calendar/v3/calendars/" + 
                calendarId + "/events/" + eventId);

        if(log_.isDebugEnabled()) 
            log_.debug("Invoking Google Calendar GET API with url [{}]", url.toString());         
        HttpRequest request = googleConfig_.getRequestFactory().buildGetRequest(url);
        HttpResponse response = request.execute();          
            
        Map<String, Object> jsonMap = processGCalResponse(response);
        if(log_.isDebugEnabled()) 
            log_.debug("Google Calendar response body [{}]", jsonMap.toString());
        
        return new ResponseEntity<>(jsonMap, HttpStatus.OK);
    }

    /*
     * (non-Javadoc)
     * @see com.crosswind.calendar.service.CalendarService#deleteEvent(java.lang.String, java.lang.String)
     */
    @Override
    public ResponseEntity<?> deleteEvent(String calendarId, String eventId) throws IOException
    {
        GenericUrl url = new GenericUrl("https://www.googleapis.com/calendar/v3/calendars/" + 
                calendarId + "/events/" + eventId);

        if(log_.isDebugEnabled()) 
            log_.debug("Invoking Google Calendar DELETE API with url [{}]", url.toString());         
        HttpRequest request = googleConfig_.getRequestFactory().buildDeleteRequest(url);
        request.execute();          
            
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /*
     * (non-Javadoc)
     * @see com.crosswind.calendar.service.CalendarService#createEvent(java.lang.String, java.util.Map)
     */
    @Override
    public ResponseEntity<?> createEvent(String calendarId, Map<String, Object> eventData) throws IOException
    {
        GenericUrl url = new GenericUrl("https://www.googleapis.com/calendar/v3/calendars/" + 
                calendarId + "/events");

        HttpContent content = new JsonHttpContent(googleConfig_.getJsonFactory(), eventData);
        if(log_.isDebugEnabled()) 
            log_.debug("Invoking Google Calendar POST API with url [{}], body [{}]",
                    url.toString(), mapper_.writeValueAsString(eventData));        
        
        HttpRequest request = googleConfig_.getRequestFactory().buildPostRequest(url, content);
        HttpResponse response = request.execute();          
            
        Map<String, Object> jsonMap = processGCalResponse(response);
        if(log_.isDebugEnabled()) 
            log_.debug("Google Calendar response body [{}]", jsonMap.toString());
        
        return new ResponseEntity<>(jsonMap, HttpStatus.CREATED);        
    }
    
    private Map<String, Object> processGCalResponse(HttpResponse response)
        throws IOException
    {
        InputStream iStream = null;
        Map<String, Object> jsonMap;
          
        try 
        {
            iStream = response.getContent();
            ObjectMapper mapper = new ObjectMapper();
            jsonMap = mapper.readValue(iStream, 
                    new TypeReference<Map<String, Object>>(){});
        } 
        finally 
        {
            if(iStream != null)
                iStream.close();
        }
        
        return jsonMap;
    }   
}
