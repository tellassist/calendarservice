/*************************************************************************
 *
 * ROUTINIFY CONFIDENTIAL
 * __________________
 *
 * [2018] - [2019] Routinify, Inc.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Routinify, Inc and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Routinify, Inc and its suppliers
 * and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Routinify, Inc.
 */

package com.crosswind.calendar.serviceImpl;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.CalendarScopes;

@Configuration
public class GoogleConfig 
{
	private Logger log_ = LoggerFactory.getLogger(GoogleConfig.class);
	
	private final String APPLICATION_NAME = "Routinify";
	private final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	private final java.io.File DATA_STORE_DIR =
		      new java.io.File(System.getProperty("user.home"), ".store/calendar_sample");
	
	private FileDataStoreFactory dataStoreFactory_;
	private HttpTransport httpTransport_;
	private HttpRequestFactory requestFactory_;
	private Credential credential_;
	private Calendar gCalclient_;
	
	@PostConstruct
	protected void postConstruct() throws Exception
	{
		log_.info("Initializing Google Calendar Service");

	    // initialize the data store factory
	    dataStoreFactory_ = new FileDataStoreFactory(DATA_STORE_DIR);

	    // initialize the transport
		httpTransport_ = GoogleNetHttpTransport.newTrustedTransport();
	
		// authorization
		credential_ = authorize(httpTransport_, dataStoreFactory_);

	    requestFactory_ = httpTransport_.createRequestFactory(
	            new HttpRequestInitializer() 
	            {
	                @Override public void initialize(HttpRequest request) 
	                        throws IOException 
	                {
	                    credential_.initialize(request);
	                    
	                    /* TODO: russ@routinify.com
	                     * Make these settings configurable
	                     */
	                    request.setReadTimeout(60000);
	                }
	            });
	    
	    // set up global Calendar instance
	    gCalclient_ = new Calendar.Builder(
	          httpTransport_, JSON_FACTORY, credential_)
	    		.setApplicationName(APPLICATION_NAME).build();				
	}
	
	public HttpRequestFactory getRequestFactory() {return requestFactory_;}
	public Credential getCredential() {return credential_;}
	public Calendar getGCalClient() {return gCalclient_;}
	public JsonFactory getJsonFactory() {return JSON_FACTORY;}
	
	private Credential authorize(HttpTransport httpTransport, FileDataStoreFactory dataStoreFactory) 
			throws Exception
	{
	    // load client secrets		
		GoogleClientSecrets clientSecrets = null;
		GoogleAuthorizationCodeFlow flow = null;
	    
		try 
	    {
			clientSecrets = GoogleClientSecrets.load(JSON_FACTORY,
			    new InputStreamReader(CalendarServiceImpl.class.getClassLoader().getResourceAsStream("credentials.json")));
			
			if(clientSecrets.getDetails().getClientId().startsWith("Enter")
			    || clientSecrets.getDetails().getClientSecret().startsWith("Enter ")) 
			{
			    log_.error("Invalid Credentials");
			}
			
			// set up authorization code flow
			flow = new GoogleAuthorizationCodeFlow.Builder(
					httpTransport, JSON_FACTORY, clientSecrets,
					Collections.singleton(CalendarScopes.CALENDAR)).setDataStoreFactory(dataStoreFactory)
			        .setAccessType("offline")					
			        .build();
			
			log_.info("Authorizing Google Calendar API");
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		// authorize
	 	return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
	}
}
