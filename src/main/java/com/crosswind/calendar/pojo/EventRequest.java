/*************************************************************************
 *
 * ROUTINIFY CONFIDENTIAL
 * __________________
 *
 * [2018] - [2019] Routinify, Inc.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Routinify, Inc and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Routinify, Inc and its suppliers
 * and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Routinify, Inc.
 */

package com.crosswind.calendar.pojo;

public class EventRequest {
	
	private String calendarId;
	private String summary;
	private String description;
	private String startdate;
	private String enddate;
	private String rrule;
	private String timezone;
	public String getCalendarId() {
		return calendarId;
	}
	public void setCalendarId(String calendarId) {
		this.calendarId = calendarId;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	public String getRrule() {
		return rrule;
	}
	public void setRrule(String rrule) {
		this.rrule = rrule;
	}
	public String getTimezone() {
		return timezone;
	}
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}
	
}
