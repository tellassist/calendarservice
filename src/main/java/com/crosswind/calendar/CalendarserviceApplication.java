/*************************************************************************
 *
 * ROUTINIFY CONFIDENTIAL
 * __________________
 *
 * [2018] - [2019] Routinify, Inc.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Routinify, Inc and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Routinify, Inc and its suppliers
 * and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Routinify, Inc.
 */

package com.crosswind.calendar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class CalendarserviceApplication 
{
	public static void main(String[] args) 
	{
		SpringApplication.run(CalendarserviceApplication.class, args);
	}
}
