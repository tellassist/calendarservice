/*************************************************************************
 *
 * ROUTINIFY CONFIDENTIAL
 * __________________
 *
 * [2018] - [2019] Routinify, Inc.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Routinify, Inc and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Routinify, Inc and its suppliers
 * and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Routinify, Inc.
 */

package com.crosswind.calendar.service;

import java.io.IOException;
import java.util.Map;

import org.springframework.http.ResponseEntity;

import com.crosswind.calendar.pojo.EventRequest;

public interface CalendarService {

	public String authorizeSeniorAndAssignCalendarId(String email) throws Exception;
	
	public void createEvent(EventRequest eventRequest) throws Exception;

    /**
     * Create a new calendar
     * 
     * @param calendarData  data passed to GCal to create the calendar
     * @return the response results from GCal API
     */
    public ResponseEntity<?> createCalendar(Map<String, Object> calendarData)
            throws IOException;

    /**
     * Delete a specific Google Calendar
     * 
     * @param calendarId  google calendar ID
     * @return the response results from GCal API
     */
    public ResponseEntity<?> deleteCalendar(String calendarId)
            throws IOException;
    
    /**
	 * Get a list of Google Calendar events
	 * 
	 * @param calendarId  google calendar ID
	 * @param params  URL query parameters
	 * @return the response results from GCal API
	 * @throws IOException
	 */
	public ResponseEntity<?> getEvents(String calendarId, Map<String, String[]> params)
			throws IOException;

	/**
	 * Get a specific Google Calendar event
	 * 
	 * @param calendarId  google calendar ID
	 * @param eventId  calendar event ID
     * @return the response results from GCal API
	 */
    public ResponseEntity<?> getEvent(String calendarId, String eventId)
            throws IOException;

    /**
     * Delete a specific Google Calendar event
     * 
     * @param calendarId  google calendar ID
     * @param eventId  calendar event ID
     * @return the response results from GCal API
     */
    public ResponseEntity<?> deleteEvent(String calendarId, String eventId)
            throws IOException;

    /**
     * Create a new calendar event
     * 
     * @param calendarId  google calendar ID
     * @param eventData  data passed to GCal to create the event
     * @return the response results from GCal API
     */
    public ResponseEntity<?> createEvent(String calendarId, Map<String, Object> eventData)
            throws IOException;
}
