export LOG4J_APPENDER_FLUENTD_HOST=localhost
java -Xmx512M -Xms512M -agentlib:jdwp=transport=dt_socket,address=18082,server=y,suspend=n -Dspring.config.location=../config/application.properties -Dlog4j.configuration=file:../config/log4j.properties -DLOG4J_APPENDER_FLUENTD_HOST=$LOG4J_APPENDER_FLUENTD_HOST -jar ../target/calendarservice-1.5.0.jar
